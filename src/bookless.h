/* File: bookless.h
 * Purpose: headers for bookless spellcasting classes
 */
 
#ifndef INCLUDED_BOOKLESS_H
#define INCLUDED_BOOKLESS_H

typedef struct spellholder {
	char *name;
	int level;
	int cost;
	int fail;
	char *desc;
	void (*spell)(int);
	char needs_dir;
} spellholder;

extern void do_cmd_bookless(cmd_code code, cmd_arg args[]);
extern void textui_cmd_bookless(void);

#endif /* INCLUDED_BOOKLESS_H */
